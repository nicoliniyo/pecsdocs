---
id: mockup
title: Mockup
sidebar_label: Mockup
---

title: Mockup
short-title: Mockup
description: Mockup.
diff2html: true


{% assign api = site.api | append: '/flutter' -%}
{% capture code -%} {{site.repo.this}}/tree/{{site.branch}}/src/_includes/code {%- endcapture -%}
{% capture examples -%} {{site.repo.this}}/tree/{{site.branch}}/examples {%- endcapture -%}
{% assign rawExFile = 'https://raw.githubusercontent.com/flutter/website/master/examples' -%}
{% capture demo -%} {{site.repo.flutter}}/tree/{{site.branch}}/dev/integration_tests/flutter_gallery/lib/demo {%- endcapture -%}

<style>dl, dd { margin-bottom: 0; }</style>

{{site.alert.secondary}}
  <h4 class="no_toc">Mockups</h4>

  * Parámetros
  * Formularios
  * Componentes del diseño
  
{{site.alert.end}}


En la manufactura y diseño, un mockup, mock-up, o maqueta es un modelo a escala o tamaño real de un diseño o un dispositivo, utilizado para la demostración, evaluación del diseño, promoción, y para otros fines. Un mockup es un prototipo si proporciona al menos una parte de la funcionalidad de un sistema y permite pruebas del diseño.1​ Los mockups son utilizados por los diseñadores principalmente para la adquisición de comentarios por parte de los usuarios. Los mock-ups abordan la idea capturada en la ingeniería popular: «Usted puede arreglarlo ahora en el dibujo con una goma de borrar o más tarde en la obra con un martillo».2

### Diseño de Pantallas

El diseño de la aplicación tendra este look and feel:

#### Dashboard
<div class="row mb-4">
  <div class="col-12 text-center">
    {% asset diseno/mockups/dashboard.png class="border mt-1 mb-1 mw-100" alt="WBS" %}
  </div>
</div>

#### Menu Lateral
<div class="row mb-4">
  <div class="col-4 text-center">
    {% asset diseno/mockups/menu-lateral.png class="border mt-1 mb-1 mw-100" alt="WBS" %}
  </div>
</div>

#### Listados
<div class="row mb-4">
  <div class="col-12 text-center">
    {% asset diseno/mockups/listados.png class="border mt-1 mb-1 mw-100" alt="WBS" %}
  </div>
</div>

#### Formularios
<div class="row mb-4">
  <div class="col-12 text-center">
    {% asset diseno/mockups/formularios.png class="border mt-1 mb-1 mw-100" alt="WBS" %}
  </div>
</div>

#### Botones
<div class="row mb-4">
  <div class="col-12 text-center">
    {% asset diseno/mockups/botones.png class="border mt-1 mb-1 mw-100" alt="WBS" %}
  </div>
</div>

#### Paneles
<div class="row mb-4">
  <div class="col-6 text-center">
    {% asset diseno/mockups/paneles.png class="border mt-1 mb-1 mw-100" alt="WBS" %}
  </div>
</div>

#### Tipografía
<div class="row mb-4">
  <div class="col-12 text-center">
    {% asset diseno/mockups/tipografia.png class="border mt-1 mb-1 mw-100" alt="WBS" %}
  </div>
</div>

#### Estadisticas
<div class="row mb-4">
  <div class="col-12 text-center">
    {% asset diseno/mockups/estadisticas.png class="border mt-1 mb-1 mw-100" alt="WBS" %}
  </div>
</div>

{{site.alert.note}}
  [Definición Completa en Wikipedia][]
{{site.alert.end}}

[Definición Completa en Wikipedia]: https://es.wikipedia.org/wiki/Mockup

