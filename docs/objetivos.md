---
id: objetivos
title: Objetivos
sidebar_label: Objetivos
---


**Contenido**

```
  Alcance del proyecto.
  Objetivos del proyecto.
  Componentes y entregables del proyecto.
```


 
## Alcance
Desarrollar las herramientas descritas y ponerlas en marcha una por una, para atender de manera mínima la necesidad
 de apoyar con terapia a pacientes del Espectro Autista dentro de casa.
 
## Objetivos
* Desarrollar un conjunto de soluciones web para el apoyo a terapias del Espectro Autista
* Todas las soluciones deben cumplir con modalidad en linea y fuera de linea, para evitar el consumo de datos
* Todas las soluciones deben cumplir ser compatibles con dispositivos mobiles
* Todas las soluciones deben cumplir con backward compatibility
* Todas las soluciones deben ser compatibles con dispositivos moviles y tablets
* Plataforma de enseñanza a distancia en linea y fuera de linea, para tecnicas de apoyo a disgnosticos TEA

### Objetivos Especificos
* Una aplicación web material PECS - en línea
* Una aplicación web material PECS - fuera de línea, que optimice el uso de datos
* Una aplicación web para reforzamiento ABA
* Sesiones de capacitación y entrenamiento de contenido multimedia o video 


### Diseño Técnico 

El producto final de software que tiene como finalidad la administración y evaluación de riesgos, es desarrollado a
 traves de un proceso ya establecido por el mercado, el cual utiliza un marco de trabajo ágil, a continuación se
  describe el detalle de este.

#### Requerimientos 
Detalle de las especificaciones que requiere un computador para poder instalar y ejecutar la aplicación de forma
 correcta.
 
  **Hardware:** Los requerimientos de hardware del computador necesarios para funcionamiento del software son:
  
   * Computador 
   * Memoria RAM
   * Espacio en disco duro
   * Tarjeta grafica
    
  **Software:** Los requerimientos de software del computador necesarios para funcionamiento del software son:
  
   * Sistema Operativo Windows 8 o superior
   * Sistema Apple MAC 10.3 o superior

#### Diseño de interfaz Gráfica

* Wireframe de las pantallas del software, un wireframe es un diseño blanco y negro, representan la organización del
 sistema en su primera etapa antes de construir las interfaces gráficas de usuario

* Mockups son pantallas que no tienen funcionalidad pero se asemejan a las pantallas que el sistema tendrá en su versión final, se presentará 1 mockup para cada ítem a continuación:

  * Parámetros (1 pantalla mockup)
  * Operación de Registro (1 pantalla mockup)
  * Resultados (1 pantalla mockup)

#### Diseño del software
El diseño se organiza a traves de funcionalidades, llamadas _Módulos_, cada módulo contiene la
 logica de negocio especifica y utiliza de parámetros del sistema o aquellos especifícos del
  módulo.
  
##### Módulo de seguridad
* Acceso de usuario por nombre de usuario y contraseña

##### Módulo de parámetros
* Parámetros del sistema

##### Módulo de configuración
* Configuración de idioma de la aplicación (Opciones: Ingles y Español)
* Configuración de periodicidad de registros (mensual, trimestral, otros)
* Configuración de Indicadores de Evaluación Social

##### Módulo de operaciones
* Diseño de Registro y Seguimiento de correspondencia
* Diseño de Registro y Edición del Modelo Base y Formulas de Evaluación e Indicadores (Indicadores que se utilizaran
 para la evaluación
* Diseño del Registro de Acciones, las acciones sirven para la Mitigación de Riesgo, cada vez que se evalúa un riesgo
 se pueden registrar las acciones a tomar y ver su efecto (positivo o negativo) dentro de un caso de riesgo elevado
* Diseño de datos de monitoreo, se ingresan diferentes datos para realizar monitoreo

##### Módulo de Resultados (Reportes y Gráficos Estadísticos)
* Reportes de monitoreo en español y sumarios en ingles
* Reportes de datos, tablas utilizadas para las evaluaciones
* Reportes de información agregada, siempre y cuando los objetivos de evaluación se hayan evaluado con el mismo modelo
 de indicadores sociales

##### Diseño de almacenamiento de información
* Listado de documentos almacenados en base de datos

#### 2. Una aplicación de escritorio desarrollada en función al documento de diseño
* Instalador para Windows, programa para instalar el software en sistema operativo Windows
* Instalador para MAC, programa para instalar en software en sistema operativo Apple MAC

#### 3. Documentación
* Documentación del proyecto
* Manuales de instalación del software, en formato digital, según la versión del software
* Manuales de usuario del software, en formato digital, según la versión del software

## Administracion del proyecto
Descripción de los servicios que TodoOpen pondrá a disposición del proyecto de desarrollo del producto:

Acceso a los documentos de planificación y del proyecto, manuales de usuario y manual técnico de instalación
Planificación del proyecto, administración de cambios y mantenimiento 
Desarrollo continuo de las aplicaciones (programación) en función a la planificación y objetivos establecidos por el cliente y definidos en diseño
Cumplir con las políticas de seguridad del cliente
Entrega de instaladores del software

## Marco de Trabajo para el proyecto - SCRUM
El inicio de todo proyecto involucra la practica de desarrollo de software bajo la metodología SCRUM caracterizada por:

Adoptar una estrategia de desarrollo incremental, donde se presentan versiones del software cada 2 semanas.
La calidad del resultado más el conocimiento tácito de los profesionales  organizados en equipos y áreas de experiencia.
Reemplazo de las diferentes fases del desarrollo, en lugar de realizar una tras otra en un ciclo secuencial o en cascada.

<div class="row mb-4">
  <div class="col-12 text-center">
    {% asset propuesta/scrum.png class="border mt-1 mb-1 mw-100" alt="WBS" %}
    <small>Marco de Trabajo SCRUM</small>
  </div>
</div>

## Aclaraciones Sobre Licencia del Producto y Propiedad Intelectual
* El cliente obtiene el producto de software desarrollado de uso ilimitado, siempre y cuando este sea de uso por la
 misma organización, institución, empresa, razón social, organización social, persona natural o jurídica a la cual fue extendida.
* TodoOpen queda exento de cualquier responsabilidad de carácter exclusiva sobre el producto y el uso del conocimiento
 adquirido en el proceso de desarrollo.
* TodoOpen no reconoce ninguna idea o lógica de negocio como valor agregado u propiedad intelectual utilizada en el
 desarrollo del producto.

## Garantía del Producto
* La garantía consiste en 40 horas a ejecutar post entrega del producto, la garantía solo cubre errores del software
 desarrollado.
* La garantía inicia a los 30 días calendario de haber entregado el software, todas las observaciones deben ser
 realizadas en función al alcance y objetivos del proyecto y dentro de este periodo.
* Todo aquello que requiera nuevas funcionalidades dentro del software y no este descrito en el documento de diseño
, no es parte de la garantía.
* Para garantizar la ejecución de la garantía, el inicio y fin de garantía, no deben existir pagos pendientes sobre el
 contrato, ni requerimientos incompletos en el software que estén descritos en el documento de diseño.

## Costo del Desarrollo del Producto
Se recomienda la siguiente forma de pago para un proyecto con un estimado en meses de desarrollo

| **Desarrollo del Software**                                     	| **Porcentaje** 	| **Monto USD** 	|
|-----------------------------------------------------------------	|:--------------:	|:-------------:	|
| A la firma del contrato y entrega del diseño                    	|      50 %      	|      9000     	|
| A 30 días y entrega de los módulos de seguridad y configuración 	|      20 %      	|      3600     	|
| A 60 días y entrega del módulo de operaciones                   	|      20 %      	|      3600     	|
| Entrega final del software                                      	|      10 %      	|      1800     	|
| Total                                                           	|                	|     18000     	|


## Soporte del Producto (post desarrollo)
Una vez terminada la fase de desarrollo y el producto esta terminado y entregado

En caso de futuras modificaciones o mantenimiento el costo de soporte, bajo las mismas características del desarrollo tiene un costo de:

| **Soporte Adicional**                  	| **Monto USD** 	|
|----------------------------------------	|:-------------:	|
| Soporte 1 Semana de trabajo extra      	|               	|
| Total 1 semana de desarrollo adicional 	| 700 USD       	|
